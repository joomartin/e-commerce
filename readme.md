**Microservices with Laravel - E-commerce**

**Install:**
1. Clone: `git clone --recursive https://gitlab.com/microservices-with-laravel/e-commerce/e-commerce.git`
2. Up: `docker-compose up`
3. Test: `curl localhost:8000/api/v1/catalog`

**Other commands:**
- Seed databases: `./bin/run-seed.sh`
- Reset databases: `./bin/reset-db.sh`
- Reset Redis:
```
docker-compose exec redis redis-cli
127.0.0.1:6379> FLUSHALL
```
- Import `api-endpoints-postman.json` into Postman to see all APIs

**Services:**
- Frontend: [http://localhost:3000](http://localhost:3000)
- Proxy: [http://localhost:8000](http://localhost:8000)
- Products API: [http://localhost:8001](http://localhost:8001)
- Ratings API: [http://localhost:8002](http://localhost:8002)
- Warehouse API: [http://localhost:8003](http://localhost:8003)
- Catalog API: [http://localhost:8004](http://localhost:8004) 
- Orders API: [http://localhost:8005](http://localhost:8005)
- MySQL: [http://localhost:33060](http://localhost:33060)
- Redis: [http://localhost:63790](http://localhost:63790)
- phpMyAdmin: [http://localhost:8080](http://localhost:8080)
- Redis commander: [http://localhost:63791](http://localhost:63791)
- You can change everything in the `docker-compose.yml`

**API Endpoints:**
- Import `api-endpoints-postman.json` into Postman
